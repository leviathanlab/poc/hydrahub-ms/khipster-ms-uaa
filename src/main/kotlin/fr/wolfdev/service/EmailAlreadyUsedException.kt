package fr.wolfdev.service

class EmailAlreadyUsedException : RuntimeException("Email is already in use!")
