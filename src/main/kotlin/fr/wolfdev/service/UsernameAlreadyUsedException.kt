package fr.wolfdev.service

class UsernameAlreadyUsedException : RuntimeException("Login name already used!")
