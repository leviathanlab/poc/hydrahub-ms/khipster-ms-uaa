package fr.wolfdev.service

class InvalidPasswordException : RuntimeException("Incorrect password")
